package com.awx.awx.kommentar;

import com.awx.awx.beitrag.BeitragRepository;
import com.awx.awx.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class KommentarController {
    @Autowired
    KommentarRepository kommentarRepository;
    @Autowired
    BeitragRepository beitragRepository;


    @PostMapping("/kommentar")
    public String createKommentar(@ModelAttribute("kommentar") @Valid KommentarDTO kommentarDTO, BindingResult bindingResult, @ModelAttribute("currentUser") User currentUser, @RequestParam("beitragId") long id) {
        if (bindingResult.hasErrors()) {
            return "redirect:/";
        }
        Kommentar kommentar = new Kommentar(kommentarDTO.getKommentarTitle(), kommentarDTO.getCreationDate(), currentUser, beitragRepository.findById(id).get());
        kommentarRepository.save(kommentar);

        return "redirect:/details?beitragId=" + id;
    }

    //kommentar Löschen === Under Construction
    @PostMapping("/kommentarLoeschen")
    public String deleteKommentar(@RequestParam("kommentarId") Kommentar kommentar) {
        kommentarRepository.deleteById(kommentar.getId());
        return "redirect:/";
    }

    //kommentar bearbeiten === Under Construction
    @PostMapping("kommentarBearbeiten")
    public String kommentarBearbeiten(@RequestParam("kommentarId") long id, @ModelAttribute("kommentar") KommentarDTO kommentarDTO) {
      Kommentar kommentar =  kommentarRepository.findById(id).get();
      kommentar.setKommentarTitle(kommentarDTO.getKommentarTitle());
      kommentar.setCreationDate(kommentarDTO.getCreationDate());
      kommentarRepository.save(kommentar);
        return "redirect:/";
    }
}