package com.awx.awx.beitrag;

import com.awx.awx.user.User;

import javax.persistence.Lob;
import java.time.Instant;


public class BeitragDTO {

    @Lob
    private String text="";
    @Lob
    private String title="";
    private Instant creationDate;

    private User user;

    public BeitragDTO() {
    }

    public BeitragDTO(String text, String title, Instant creationDate, User user) {
        this.text = text;
        this.title = title;
        this.creationDate = Instant.now();
       this.user = user;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
