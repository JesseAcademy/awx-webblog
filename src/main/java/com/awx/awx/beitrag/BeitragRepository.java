
package com.awx.awx.beitrag;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BeitragRepository  extends JpaRepository <Beitrag, Long> {
    List<Beitrag> findAllByOrderByIdDesc(); //Besser Erstellungsdatum
    List<Beitrag> findByTitle(String title);
    List<Beitrag> findAll();

    void deleteById(long id);
}
