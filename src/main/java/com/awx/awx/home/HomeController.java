package com.awx.awx.home;

import com.awx.awx.beitrag.BeitragRepository;
import com.awx.awx.user.UserRepository;
import com.awx.awx.session.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @Autowired
    private BeitragRepository beitragRepository;

    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    UserRepository userRepository;


    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("user", userRepository.findAllByAdminFalse());  //wurde von UserController hinzugefügt, da dies hier die zentrale Methode für den home-screen ist
        model.addAttribute("beitraege", beitragRepository.findAllByOrderByIdDesc());

        return "home";
    }

}
