package com.awx.awx.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @GetMapping("/makeAdmin/{id}")
    public String userZuAdmin(@PathVariable (name="id") Long id) {
        User selectedUser = userRepository.findById(id).get();
        userService.ernenneUserzuAdmin(selectedUser);

        return "redirect:/";
    }

}