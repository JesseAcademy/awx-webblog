Woche 6 - 1. WebBlog
Vorgaben: WebBlog in Gruppenarbeit (3 Personen) nach Scrum mit Spring Boot, Datenbank und per Thymeleaf-Verknüpfung zum Frontend.
Thema (selbst gewählt): Verschwörungstheorien
User Stories, s.u., wurden erfüllt und nach Priorität von oben nach unten abgearbeitet.


User Stories: Blog
-Als Nutzer kann ich die Beiträge des Blogs in chronologischer Reihenfolge (neueste zuerst)
lesen, um Einblick in die Gedanken der Administratoren zu erhalten.
-Als Nutzer kann ich die Kommentare zu einem beitrag in chronologischer Reihenfolge (älteste
zuerst) lesen, um Einblick in die Meinungen der Nutzer zu erhalten.
-Als anonymer Nutzer kann ich mich mit Benutzername und Passwort registrieren, damit meine
Identität etabliert wird.
-Als anonymer Nutzer kann ich mich mit vorher registrierten Benutzername und Passwort
einloggen, um registrierter Nutzer zu werden.
-Als registrierter Nutzer kann ich Kommentare zu einzelnen Beiträgen schreiben um mit den
Administratoren und anderen Nutzern meine Meinung zum Thema des Beitrags zu teilen.
-Als Administrator kann ich registrierte Nutzer zu Administratoren benennen, damit sie mich bei
bei Redaktion und Verwaltung des Blogs unterstützen können.
-Als Administrator kann ich neue Beiträge in dem Blog erstellen, um meine Gedanken mit der
Welt zu teilen. Beiträge haben eine Überschrift und einen Inhalt (beides nur Text).
-Als registrierter Nutzer kann ich von mir erstellte Kommentare löschen, um meiner geänderten
Meinung gerecht zu werden.
-Als Administrator kann ich Kommentare löschen, um die Qualität des Inhalts meines Blogs
sicher zu stellen.
-Als Administrator kann ich Beiträge bearbeiten, um Fehler auszubessern.
-Als Administrator kann ich Beiträge inklusiver der zugehörigen Kommentare löschen, um grobe
Fehler zu korrigieren.
